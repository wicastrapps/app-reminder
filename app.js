var WebSocketServer = require("ws").Server,
    http = require('http'),
    fs = require('fs'),
    // NEVER use a Sync function except at start-up!
    index = fs.readFileSync(__dirname + '/index.html');

// Send index.html to all requests
var app = http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(index);
});

var wss = new WebSocketServer({server: app});

// Send current time to all connected clients
function sendTime(ws) {
    console.log("Sending time...");

    var msg = { message: 'time', time: new Date().toJSON() };

    ws.send(JSON.stringify(msg));
}

var timeInt;

wss.on('connection', function(ws) {
    ws.on('close', function(){
        clearInterval(timeInt);
    });

    ws.on('message', function(message) {
        console.log('Msg received from client');
        console.log(message);
    });

    console.log("Sending welcome message...");

    var msg = { 'message': 'welcome', text: 'Welcome!' };
    ws.send(JSON.stringify(msg));

    // Send current time every 10 secs
    timeInt = setInterval(function(){
        sendTime(ws);
    }, 10000);
});

app.listen(3000);